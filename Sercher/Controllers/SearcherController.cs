﻿using PeopleSearcher.DAL;
using Sercher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sercher.Controllers
{
    public class SearcherController : ApiController
    {
        public IEnumerable<Customer> GetHumans([FromUri] SearchFilter searchFilter)
        {
            using (SearcherContext sc = new SearcherContext())
            {
                IEnumerable<Human> humansByNames = from human in sc.Humen
                                    join address in sc.Addresses on human.id equals address.id
                                    where human.Name.Contains(searchFilter.Name)
|| human.Surename.Contains(searchFilter.Surename) || human.Patronymyc.Contains(searchFilter.Patronymyc) || address.TheAddress.Contains(searchFilter.Address)
                                    select human;
                return humansByNames.Select(h => new Customer(h)).ToList();
            }

        }
    }
}
