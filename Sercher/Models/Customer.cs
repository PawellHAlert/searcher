﻿using PeopleSearcher.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sercher.Models
{
    public class Customer
    {
        public Customer(Human human)
        {
            Name = human.Name;
            Surename = human.Surename;
            Patronymyc = human.Patronymyc;
            Addresses = new List<string>();
            foreach(var addr in human.Adresses)
            {
                Addresses.Add(addr.TheAddress);
            }
        }
        public string Name { get; set; }
        public string Surename { get; set; }
        public string Patronymyc { get; set; }
        public ICollection<string> Addresses { get; set; }
    }
}