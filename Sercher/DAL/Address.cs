﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PeopleSearcher.DAL
{
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public string TheAddress { get; set; }
        public long HumanId { get; set; }
        public virtual Human Human {get;set;}
    }
}