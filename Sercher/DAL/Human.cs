﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PeopleSearcher.DAL
{
    public class Human
    {
        public Human()
        {
            Adresses = new List<Address>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public string Name { get; set; }
        public string Surename { get; set; }
        public string Patronymyc { get; set; }
        public  virtual ICollection<Address> Adresses { get; set; }
    }
}