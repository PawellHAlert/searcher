﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PeopleSearcher.DAL;

namespace PeopleSearcher.DAL
{
    public class SearcherContext : DbContext
    {
        static SearcherContext()
        {
            Database.SetInitializer<SearcherContext>(new ContextInitializer());
        }
        public DbSet<Human> Humen { get; set; }
        public DbSet<Address> Addresses { get; set; }
    }
}