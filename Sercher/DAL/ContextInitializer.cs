﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PeopleSearcher.DAL
{
    public class ContextInitializer : DropCreateDatabaseAlways<SearcherContext>
    {
        protected override void Seed(SearcherContext context)
        {
            var address1 = new Address() { TheAddress = "Урюпинск" };
            var address2 = new Address() { TheAddress = "Ухрюпинск" };
            var address3 = new Address() { TheAddress = "Цетральная африка" };
            var address4 = new Address() { TheAddress = "сеыерный полюс" };

            var customer1 = new Human() { Name = "Иванов", Patronymyc = "Иванович", Surename = "Пингвин" };
            customer1.Adresses.Add(address3);
            customer1.Adresses.Add(address4);
            var customer2 = new Human() { Name = "Петров", Patronymyc = "Петр", Surename = "Перович" };
            customer2.Adresses.Add(address2);
            var customer3 = new Human() { Name = "Креветка", Patronymyc = "Боеврй", Surename = "Помидор" };
            customer3.Adresses.Add(address1);

            context.Humen.AddRange(new []{customer1,customer2,customer3});
            context.SaveChanges();

        }
    }
}